
from ctypes import *
from .arnold_common import ai, NullToNone
from .ai_color import *
from .ai_matrix import *
from .ai_node_entry import *
from .ai_vector import *
from .ai_types import *

class AtNode(Structure):
    pass

class AtUserParamIterator(Structure):
    pass

_AiNode = ai.AiNode
_AiNode.argtypes = [AtString, AtString, POINTER(AtNode)]
_AiNode.restype = c_void_p

def AiNode(nentry_name, name = "", parent = None):
    return NullToNone(_AiNode(nentry_name, name, parent), POINTER(AtNode))

_AiNodeLookUpByName = ai.AiNodeLookUpByName
_AiNodeLookUpByName.argtypes = [AtString, POINTER(AtNode)]
_AiNodeLookUpByName.restype = c_void_p

def AiNodeLookUpByName(name, parent = None):
    return NullToNone(_AiNodeLookUpByName(name, parent), POINTER(AtNode))

AiNodeReset = ai.AiNodeReset
AiNodeReset.argtypes = [POINTER(AtNode)]

AiNodeResetParameter = ai.AiNodeResetParameter
AiNodeResetParameter.argtypes = [POINTER(AtNode), AtPythonString]

_AiNodeClone = ai.AiNodeClone
_AiNodeClone.argtypes = [POINTER(AtNode), AtString, POINTER(AtNode)]
_AiNodeClone.restype = c_void_p

def AiNodeClone(node, new_name = "", parent = None):
    return NullToNone(_AiNodeClone(node, new_name, parent), POINTER(AtNode))

AiNodeDestroy = ai.AiNodeDestroy
AiNodeDestroy.argtypes = [POINTER(AtNode)]
AiNodeDestroy.restype = c_bool

AiNodeIs = ai.AiNodeIs
AiNodeIs.argtypes = [POINTER(AtNode), AtString]
AiNodeIs.restype = c_bool

AiNodeDeclare = ai.AiNodeDeclare
AiNodeDeclare.argtypes = [POINTER(AtNode), AtString, AtPythonString]
AiNodeDeclare.restype = c_bool

AiNodeLink = ai.AiNodeLink
AiNodeLink.argtypes = [POINTER(AtNode), AtPythonString, POINTER(AtNode)]
AiNodeLink.restype = c_bool

AiNodeLinkOutput = ai.AiNodeLinkOutput
AiNodeLinkOutput.argtypes = [POINTER(AtNode), AtPythonString, POINTER(AtNode), AtPythonString]
AiNodeLinkOutput.restype = c_bool

AiNodeUnlink = ai.AiNodeUnlink
AiNodeUnlink.argtypes = [POINTER(AtNode), AtPythonString]
AiNodeUnlink.restype = c_bool

AiNodeIsLinked= ai.AiNodeIsLinked
AiNodeIsLinked.argtypes = [POINTER(AtNode), AtPythonString]
AiNodeIsLinked.restype = c_bool

_AiNodeGetLink = ai.AiNodeGetLink
_AiNodeGetLink.argtypes = [POINTER(AtNode), AtPythonString, POINTER(c_int)]
_AiNodeGetLink.restype = c_void_p

def AiNodeGetLink(node, input, comp = None):
    return NullToNone(_AiNodeGetLink(node, input, comp), POINTER(AtNode))

_AiNodeGetName = ai.AiNodeGetName
_AiNodeGetName.argtypes = [POINTER(AtNode)]
_AiNodeGetName.restype = AtPythonString

def AiNodeGetName(node):
    return AtPythonStringToStr(_AiNodeGetName(node))

_AiNodeGetNodeEntry = ai.AiNodeGetNodeEntry
_AiNodeGetNodeEntry.argtypes = [POINTER(AtNode)]
_AiNodeGetNodeEntry.restype = c_void_p

def AiNodeGetNodeEntry(node):
    return NullToNone(_AiNodeGetNodeEntry(node), POINTER(AtNodeEntry))

AiNodeGetLocalData = ai.AiNodeGetLocalData
AiNodeGetLocalData.argtypes = [POINTER(AtNode)]
AiNodeGetLocalData.restype = c_void_p

AiNodeSetLocalData = ai.AiNodeSetLocalData
AiNodeSetLocalData.argtypes = [POINTER(AtNode), c_void_p]

_AiNodeLookUpUserParameter = ai.AiNodeLookUpUserParameter
_AiNodeLookUpUserParameter.argtypes = [POINTER(AtNode), AtString]
_AiNodeLookUpUserParameter.restype = c_void_p

def AiNodeLookUpUserParameter(node, param):
    return NullToNone(_AiNodeLookUpUserParameter(node, param), POINTER(AtUserParamEntry))

_AiNodeGetUserParamIterator = ai.AiNodeGetUserParamIterator
_AiNodeGetUserParamIterator.argtypes = [POINTER(AtNode)]
_AiNodeGetUserParamIterator.restype = c_void_p

def AiNodeGetUserParamIterator(node):
    return NullToNone(_AiNodeGetUserParamIterator(node), POINTER(AtUserParamIterator))

AiUserParamIteratorDestroy = ai.AiUserParamIteratorDestroy
AiUserParamIteratorDestroy.argtypes = [POINTER(AtUserParamIterator)]

_AiUserParamIteratorGetNext = ai.AiUserParamIteratorGetNext
_AiUserParamIteratorGetNext.argtypes = [POINTER(AtUserParamIterator)]
_AiUserParamIteratorGetNext.restype = c_void_p

def AiUserParamIteratorGetNext(iter):
    return NullToNone(_AiUserParamIteratorGetNext(iter), POINTER(AtUserParamEntry))

AiUserParamIteratorFinished = ai.AiUserParamIteratorFinished
AiUserParamIteratorFinished.argtypes = [POINTER(AtUserParamIterator)]
AiUserParamIteratorFinished.restype = c_bool

# Parameter Writers
#
AiNodeSetByte = ai.AiNodeSetByte
AiNodeSetByte.argtypes = [POINTER(AtNode), AtString, c_ubyte]

AiNodeSetInt = ai.AiNodeSetInt
AiNodeSetInt.argtypes = [POINTER(AtNode), AtString, c_int]

AiNodeSetUInt = ai.AiNodeSetUInt
AiNodeSetUInt.argtypes = [POINTER(AtNode), AtString, c_uint]

AiNodeSetBool = ai.AiNodeSetBool
AiNodeSetBool.argtypes = [POINTER(AtNode), AtString, c_bool]

AiNodeSetFlt = ai.AiNodeSetFlt
AiNodeSetFlt.argtypes = [POINTER(AtNode), AtString, c_float]

AiNodeSetRGB = ai.AiNodeSetRGB
AiNodeSetRGB.argtypes = [POINTER(AtNode), AtString, c_float, c_float, c_float]

AiNodeSetRGBA = ai.AiNodeSetRGBA
AiNodeSetRGBA.argtypes = [POINTER(AtNode), AtString, c_float, c_float, c_float, c_float]

AiNodeSetVec = ai.AiNodeSetVec
AiNodeSetVec.argtypes = [POINTER(AtNode), AtString, c_float, c_float, c_float]

AiNodeSetVec2 = ai.AiNodeSetVec2
AiNodeSetVec2.argtypes = [POINTER(AtNode), AtString, c_float, c_float]

AiNodeSetStr = ai.AiNodeSetStr
AiNodeSetStr.argtypes = [POINTER(AtNode), AtString, AtString]

AiNodeSetPtr = ai.AiNodeSetPtr
AiNodeSetPtr.argtypes = [POINTER(AtNode), AtString, c_void_p]

AiNodeSetArray = ai.AiNodeSetArray
AiNodeSetArray.argtypes = [POINTER(AtNode), AtString, POINTER(AtArray)]

AiNodeSetMatrix = ai.AiNodeSetMatrix
AiNodeSetMatrix.argtypes = [POINTER(AtNode), AtString, AtMatrix]

# Parameter Readers
#

AiNodeGetByte = ai.AiNodeGetByte
AiNodeGetByte.argtypes = [POINTER(AtNode), AtString]
AiNodeGetByte.restype = c_ubyte

AiNodeGetInt = ai.AiNodeGetInt
AiNodeGetInt.argtypes = [POINTER(AtNode), AtString]
AiNodeGetInt.restype = c_int

AiNodeGetUInt = ai.AiNodeGetUInt
AiNodeGetUInt.argtypes = [POINTER(AtNode), AtString]
AiNodeGetUInt.restype = c_uint

AiNodeGetBool = ai.AiNodeGetBool
AiNodeGetBool.argtypes = [POINTER(AtNode), AtString]
AiNodeGetBool.restype = c_bool

AiNodeGetFlt = ai.AiNodeGetFlt
AiNodeGetFlt.argtypes = [POINTER(AtNode), AtString]
AiNodeGetFlt.restype = c_float

AiNodeGetRGB = ai.AiNodeGetRGB
AiNodeGetRGB.argtypes = [POINTER(AtNode), AtString]
AiNodeGetRGB.restype = AtRGB

AiNodeGetRGBA = ai.AiNodeGetRGBA
AiNodeGetRGBA.argtypes = [POINTER(AtNode), AtString]
AiNodeGetRGBA.restype = AtRGBA

AiNodeGetVec = ai.AiNodeGetVec
AiNodeGetVec.argtypes = [POINTER(AtNode), AtString]
AiNodeGetVec.restype = AtVector

_AiNodeGetVec2 = ai.AiNodeGetVec2
_AiNodeGetVec2.argtypes = [POINTER(AtNode), AtString]
if return_small_struct_exception:
    _AiNodeGetVec2.restype = AtVector
    def AiNodeGetVec2(node, param):
        tmp = _AiNodeGetVec2(node, param)
        return AtVector2(tmp.x, tmp.y)
else:
    _AiNodeGetVec2.restype = AtVector2
    AiNodeGetVec2 = _AiNodeGetVec2

_AiNodeGetStr = ai.AiNodeGetStr
_AiNodeGetStr.argtypes = [POINTER(AtNode), AtString]
_AiNodeGetStr.restype = AtStringReturn

def AiNodeGetStr(node, param):
    return AtStringToStr(_AiNodeGetStr(node, param))

AiNodeGetPtr = ai.AiNodeGetPtr
AiNodeGetPtr.argtypes = [POINTER(AtNode), AtString]
AiNodeGetPtr.restype = c_void_p

_AiNodeGetArray = ai.AiNodeGetArray
_AiNodeGetArray.argtypes = [POINTER(AtNode), AtString]
_AiNodeGetArray.restype = c_void_p

def AiNodeGetArray(node, param):
    return NullToNone(_AiNodeGetArray(node, param), POINTER(AtArray))

AiNodeGetMatrix = ai.AiNodeGetMatrix
AiNodeGetMatrix.argtypes = [POINTER(AtNode), AtString]
AiNodeGetMatrix.restype = AtMatrix

AiNodeSetAttributes = ai.AiNodeSetAttributes
AiNodeSetAttributes.argtypes = [POINTER(AtNode), AtPythonString]

AiNodeSetDisabled = ai.AiNodeSetDisabled
AiNodeSetDisabled.argtypes = [POINTER(AtNode), c_bool]

AiNodeIsDisabled = ai.AiNodeIsDisabled
AiNodeIsDisabled.argtypes = [POINTER(AtNode)]
AiNodeIsDisabled.restype = c_bool
