
from ctypes import *
from .arnold_common import ai
from .ai_types import *
from .ai_array import *
from .ai_bbox import *

# Volume file querying

_AiVolumeFileGetChannels = ai.AiVolumeFileGetChannels
_AiVolumeFileGetChannels.argtypes = [AtPythonString]
_AiVolumeFileGetChannels.restype = c_void_p

def AiVolumeFileGetChannels(filename):
    return NullToNone(_AiVolumeFileGetChannels(filename), POINTER(AtArray))

AiVolumeFileGetBBox = ai.AiVolumeFileGetBBox
AiVolumeFileGetBBox.argtypes = [AtPythonString, POINTER(AtArray)]
AiVolumeFileGetBBox.restype = AtBBox

