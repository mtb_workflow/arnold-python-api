
# TODO: Implement all matrix functions

from ctypes import *
from .arnold_common import ai
from .ai_types import *
from .ai_vector import *

class AtMatrix(Structure):
    _fields_ = [("data", (c_float * 4) * 4)]

    def __getitem__(self, index):
        return self.data[index]

AiM4Identity = ai.AiM4Identity
AiM4Identity.restype = AtMatrix

AiM4IsIdentity = ai.AiM4IsIdentity
AiM4IsIdentity.argtypes = [POINTER(AtMatrix)]
AiM4IsIdentity.restype = c_bool

AiM4IsSingular = ai.AiM4IsSingular
AiM4IsSingular.argtypes = [POINTER(AtMatrix)]
AiM4IsSingular.restype = c_bool

AiM4PointByMatrixMult = ai.AiM4PointByMatrixMult
AiM4PointByMatrixMult.argtypes = [POINTER(AtMatrix), POINTER(AtVector)]
AiM4PointByMatrixMult.restype = AtMatrix

AiM4VectorByMatrixMult = ai.AiM4VectorByMatrixMult
AiM4VectorByMatrixMult.argtypes = [POINTER(AtMatrix), POINTER(AtVector)]
AiM4VectorByMatrixMult.restype = AtMatrix

AiM4Translation = ai.AiM4Translation
AiM4Translation.argtypes = [POINTER(AtVector)]
AiM4Translation.restype = AtMatrix

AiM4RotationX = ai.AiM4RotationX
AiM4RotationX.argtypes = [c_float]
AiM4RotationX.restype = AtMatrix

AiM4RotationY = ai.AiM4RotationY
AiM4RotationY.argtypes = [c_float]
AiM4RotationY.restype = AtMatrix

AiM4RotationZ = ai.AiM4RotationZ
AiM4RotationZ.argtypes = [c_float]
AiM4RotationZ.restype = AtMatrix

AiM4Scaling = ai.AiM4Scaling
AiM4Scaling.argtypes = [POINTER(AtVector)]
AiM4RotationZ.restype = AtMatrix

AiM4Frame = ai.AiM4Frame
AiM4Frame.argtypes = [POINTER(AtVector), POINTER(AtVector), POINTER(AtVector), POINTER(AtVector)]
AiM4Frame.restype = AtMatrix

AiM4HPointByMatrixMult = ai.AiM4HPointByMatrixMult
AiM4HPointByMatrixMult.argtypes = [POINTER(AtMatrix), POINTER(AtHPoint)]
AiM4HPointByMatrixMult.restype = AtMatrix

AiM4VectorByMatrixTMult = ai.AiM4VectorByMatrixTMult
AiM4VectorByMatrixTMult.argtypes = [POINTER(AtMatrix), POINTER(AtVector)]
AiM4VectorByMatrixTMult.restype = AtMatrix

AiM4Mult = ai.AiM4Mult
AiM4Mult.argtypes = [POINTER(AtMatrix), POINTER(AtMatrix)]
AiM4Mult.restype = AtMatrix

AiM4Transpose = ai.AiM4Transpose
AiM4Transpose.argtypes = [POINTER(AtMatrix)]
AiM4Transpose.restype = AtMatrix

AiM4Invert = ai.AiM4Invert
AiM4Invert.argtypes = [POINTER(AtMatrix)]
AiM4Invert.restype = AtMatrix

AiM4Determinant = ai.AiM4Determinant
AiM4Determinant.argtypes = [POINTER(AtMatrix)]
AiM4Determinant.restype = c_double

AiM4Lerp = ai.AiM4Lerp
AiM4Lerp.argtypes = [c_float, POINTER(AtMatrix), POINTER(AtMatrix)]
AiM4Lerp.restype = AtMatrix
