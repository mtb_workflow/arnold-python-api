
from ctypes import *
from .arnold_common import ai, NullToNone
from .ai_color import *
from .ai_types import *
from .ai_vector import *
from .ai_matrix import *

class AtArray(Structure):
   pass

ai.AiArray.restype = POINTER(AtArray)

def AiArray(nelems, keys, type, *params):
    return ai.AiArray(nelems, keys, type, *params)

_AiArrayAllocate = ai.AiArrayAllocate
_AiArrayAllocate.argtypes = [c_uint32, c_ubyte, c_ubyte]
_AiArrayAllocate.restype = c_void_p

def AiArrayAllocate(nelements, nkeys, type):
    return NullToNone(_AiArrayAllocate(nelements, nkeys, type), POINTER(AtArray))

AiArrayDestroy = ai.AiArrayDestroy
AiArrayDestroy.argtypes = [POINTER(AtArray)]

_AiArrayConvert = ai.AiArrayConvert
_AiArrayConvert.argtypes = [c_uint32, c_ubyte, c_ubyte, c_void_p]
_AiArrayConvert.restype = c_void_p

def AiArrayConvert(nelements, nkeys, type, data):
    return NullToNone(_AiArrayConvert(nelements, nkeys, type, data), POINTER(AtArray))

AiArrayResize = ai.AiArrayResize
AiArrayResize.argtypes = [POINTER(AtArray), c_uint32, c_ubyte]

_AiArrayCopy = ai.AiArrayCopy
_AiArrayCopy.argtypes = [POINTER(AtArray)]
_AiArrayCopy.restype = c_void_p

def AiArrayCopy(array):
    return NullToNone(_AiArrayCopy(array), POINTER(AtArray))

AiArraySetKey = ai.AiArraySetKey
AiArraySetKey.argtypes = [POINTER(AtArray), c_ubyte, c_void_p]
AiArraySetKey.restype = c_bool

AiArrayMap = ai.AiArrayMap
AiArrayMap.argtypes = [POINTER(AtArray)]
AiArrayMap.restype = c_void_p

AiArrayMapKey = ai.AiArrayMapKey
AiArrayMapKey.argtypes = [POINTER(AtArray), c_ubyte]
AiArrayMapKey.restype = c_void_p

AiArrayUnmap = ai.AiArrayUnmap
AiArrayUnmap.argtypes = [POINTER(AtArray)]

AiArrayGetNumElements = ai.AiArrayGetNumElements
AiArrayGetNumElements.argtypes = [POINTER(AtArray)]
AiArrayGetNumElements.restype = c_uint32

AiArrayGetNumKeys = ai.AiArrayGetNumKeys
AiArrayGetNumKeys.argtypes = [POINTER(AtArray)]
AiArrayGetNumKeys.restype = c_ubyte

AiArrayGetType = ai.AiArrayGetType
AiArrayGetType.argtypes = [POINTER(AtArray)]
AiArrayGetType.restype = c_ubyte

AiArrayGetDataSize = ai.AiArrayGetDataSize
AiArrayGetDataSize.argtypes = [POINTER(AtArray)]
AiArrayGetDataSize.restype = c_size_t

AiArrayGetKeySize = ai.AiArrayGetKeySize
AiArrayGetKeySize.argtypes = [POINTER(AtArray)]
AiArrayGetKeySize.restype = c_size_t

AiArrayInterpolateVec = ai.AiArrayInterpolateVec
AiArrayInterpolateVec.argtypes = [POINTER(AtArray), c_float, c_uint32]
AiArrayInterpolateVec.restype = AtVector

AiArrayInterpolateFlt = ai.AiArrayInterpolateFlt
AiArrayInterpolateFlt.argtypes = [POINTER(AtArray), c_float, c_uint32]
AiArrayInterpolateFlt.restype = c_float

AiArrayInterpolateRGB = ai.AiArrayInterpolateRGB
AiArrayInterpolateRGB.argtypes = [POINTER(AtArray), c_float, c_uint32]
AiArrayInterpolateRGB.restype = AtRGB

AiArrayInterpolateRGBA = ai.AiArrayInterpolateRGBA
AiArrayInterpolateRGBA.argtypes = [POINTER(AtArray), c_float, c_uint32]
AiArrayInterpolateRGBA.restype = AtRGBA

AiArrayInterpolateMtx = ai.AiArrayInterpolateMtx
AiArrayInterpolateMtx.argtypes = [POINTER(AtArray), c_float, c_uint32]
AiArrayInterpolateMtx.restype = AtMatrix

# AtArray getters
#
AiArrayGetBool = ai.AiArrayGetBoolFunc
AiArrayGetBool.argtypes = [POINTER(AtArray), c_uint32]
AiArrayGetBool.restype = c_bool

AiArrayGetByte = ai.AiArrayGetByteFunc
AiArrayGetByte.argtypes = [POINTER(AtArray), c_uint32]
AiArrayGetByte.restype = c_ubyte

AiArrayGetInt = ai.AiArrayGetIntFunc
AiArrayGetInt.argtypes = [POINTER(AtArray), c_uint32]
AiArrayGetInt.restype = c_int

AiArrayGetUInt = ai.AiArrayGetUIntFunc
AiArrayGetUInt.argtypes = [POINTER(AtArray), c_uint32]
AiArrayGetUInt.restype = c_uint

AiArrayGetFlt = ai.AiArrayGetFltFunc
AiArrayGetFlt.argtypes = [POINTER(AtArray), c_uint32]
AiArrayGetFlt.restype = c_float

_AiArrayGetVec2 = ai.AiArrayGetVec2Func
_AiArrayGetVec2.argtypes = [POINTER(AtArray), c_uint32]
if return_small_struct_exception:
    _AiArrayGetVec2.restype = AtVector
    def AiArrayGetVec2(array, index):
        tmp = _AiArrayGetVec2(array, index)
        return AtVector2(tmp.x, tmp.y)
else:
    _AiArrayGetVec2.restype = AtVector2
    AiArrayGetVec2 = _AiArrayGetVec2

AiArrayGetVec = ai.AiArrayGetVecFunc
AiArrayGetVec.argtypes = [POINTER(AtArray), c_uint32]
AiArrayGetVec.restype = AtVector

AiArrayGetRGB = ai.AiArrayGetRGBFunc
AiArrayGetRGB.argtypes = [POINTER(AtArray), c_uint32]
AiArrayGetRGB.restype = AtRGB

AiArrayGetRGBA = ai.AiArrayGetRGBAFunc
AiArrayGetRGBA.argtypes = [POINTER(AtArray), c_uint32]
AiArrayGetRGBA.restype = AtRGBA

AiArrayGetPtr = ai.AiArrayGetPtrFunc
AiArrayGetPtr.argtypes = [POINTER(AtArray), c_uint32]
AiArrayGetPtr.restype = c_void_p

_AiArrayGetStr = ai.AiArrayGetStrFunc
_AiArrayGetStr.argtypes = [POINTER(AtArray), c_uint32]
_AiArrayGetStr.restype = AtStringReturn

def AiArrayGetStr(array, index):
    return AtStringToStr(_AiArrayGetStr(array, index))

_AiArrayGetArray = ai.AiArrayGetArrayFunc
_AiArrayGetArray.argtypes = [POINTER(AtArray), c_uint32]
_AiArrayGetArray.restype = c_void_p

def AiArrayGetArray(array, index):
    return NullToNone(_AiArrayGetArray(array, index), POINTER(AtArray))

AiArrayGetMtx = ai.AiArrayGetMtxFunc
AiArrayGetMtx.argtypes = [POINTER(AtArray), c_uint32]
AiArrayGetMtx.restype = AtMatrix

# AtArray setters
#

AiArraySetBool = ai.AiArraySetBoolFunc
AiArraySetBool.argtypes = [POINTER(AtArray), c_uint32, c_bool]

AiArraySetByte = ai.AiArraySetByteFunc
AiArraySetByte.argtypes = [POINTER(AtArray), c_uint32, c_ubyte]

AiArraySetInt = ai.AiArraySetIntFunc
AiArraySetInt.argtypes = [POINTER(AtArray), c_uint32, c_int]

AiArraySetUInt = ai.AiArraySetUIntFunc
AiArraySetUInt.argtypes = [POINTER(AtArray), c_uint32, c_uint]

AiArraySetFlt = ai.AiArraySetFltFunc
AiArraySetFlt.argtypes = [POINTER(AtArray), c_uint32, c_float]

AiArraySetRGB = ai.AiArraySetRGBFunc
AiArraySetRGB.argtypes = [POINTER(AtArray), c_uint32, AtRGB]

AiArraySetRGBA = ai.AiArraySetRGBAFunc
AiArraySetRGBA.argtypes = [POINTER(AtArray), c_uint32, AtRGBA]

AiArraySetVec2 = ai.AiArraySetVec2Func
AiArraySetVec2.argtypes = [POINTER(AtArray), c_uint32, AtVector2]

AiArraySetVec = ai.AiArraySetVecFunc
AiArraySetVec.argtypes = [POINTER(AtArray), c_uint32, AtVector]

AiArraySetMtx = ai.AiArraySetMtxFunc
AiArraySetMtx.argtypes = [POINTER(AtArray), c_uint32, AtMatrix]

AiArraySetStr = ai.AiArraySetStrFunc
AiArraySetStr.argtypes = [POINTER(AtArray), c_uint32, AtString]

AiArraySetPtr = ai.AiArraySetPtrFunc
AiArraySetPtr.argtypes = [POINTER(AtArray), c_uint32, c_void_p]

AiArraySetArray = ai.AiArraySetArrayFunc
AiArraySetArray.argtypes = [POINTER(AtArray), c_uint32, POINTER(AtArray)]
