
from ctypes import *
from .arnold_common import ai
from .ai_color import *
from .ai_nodes import *
from .ai_vector import *

AiIrradiance = ai.AiIrradiance
AiIrradiance.argtypes = [POINTER(AtVector), POINTER(AtVector), c_ubyte, c_uint32]
AiIrradiance.restype = AtRGB

AiRadiance = ai.AiRadiance
AiRadiance.argtypes = [POINTER(AtVector), POINTER(AtVector), POINTER(AtVector), POINTER(AtNode), c_uint32, c_float, c_float, POINTER(AtNode), c_ubyte, c_uint32]
AiRadiance.restype = AtRGB
