
from ctypes import *
from .arnold_common import ai
from .ai_types import *

AtEnum = POINTER(AtPythonString)

AiEnumGetValue = ai.AiEnumGetValue
AiEnumGetValue.argtypes = [AtEnum, AtPythonString]
AiEnumGetValue.restype = c_int

_AiEnumGetString = ai.AiEnumGetString
_AiEnumGetString.argtypes = [AtEnum, c_int]
_AiEnumGetString.restype = AtPythonString

def AiEnumGetString(enum_type, index):
    return AtPythonStringToStr(_AiEnumGetString(enum_type, index))
